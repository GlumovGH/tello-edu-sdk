#!/usr/bin/python3

import socket
import time

host = ''
port = 9000
locaddr = (host,port) 

# Create a UDP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

sock.bind(locaddr)
tello_address = ('192.168.10.1', 8889)
while True: 
    try:
        msg = input("");
        if not msg:
            break  
        if 'end' in msg:
            print ('...')
            sock.close()  
            break
        msg = msg.encode(encoding="utf-8") 
        sent = sock.sendto(msg, tello_address)
    except KeyboardInterrupt:
        print ('\n . . .\n')
        sock.close()  
        break
